#-------------------------------------------------
#
# Project created by QtCreator 2013-06-30T11:21:30
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = QtHelloWorld
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
DESTDIR = bin
